// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define([], function() {
    return {
        twistPage: function() {
            var walker = document.createTreeWalker(
                document.body, 
                NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT, 
                (e) => (e.tagName == 'SCRIPT' || e.tagName == 'PRE' ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT)
            );

            var node;
            var textNodes = [];

            while (node = walker.nextNode()) {
                if (node.nodeValue != null) {
                    if (node.nodeValue.trim().length > 0) {
                        textNodes.push(node);
                    }
                } else {
                    node.normalize();
                }
            }
            var colors = [
                'aquamarine',
                'black',
                'blue',
                'blueviolet',
                'brown',
                'charcoal',
                'chartreuse',
                'coral',
                'crimson',
                'darkblue',
                'darkgray',
                'darkgreen',
                'gold',
                'goldenrod',
                'indigo',
                'khaki',
                'maroon',
                'midnightblue',
                'orange',
                'pink',
                'purple',
                'red',
                'royalblue',
                'salmon',
                'seagreen',
            ];
            textNodes.forEach(function(node) {      
                var $node = jQuery(node);
                var words = [];
                node.nodeValue.split(' ').forEach(function(word) {
                    words.push('<span style="font-size:' + Math.round(Math.random()*20+12) + 'px; font-weight:' + Math.round(Math.random()*500+200) + '; color:' + colors[Math.floor(Math.random()*colors.length)] + '; text-transform:' + ['capitalize', 'uppercase', 'none', 'none'][Math.floor(Math.random()*4)] + '">' + word + '</span>');
                    words.push('<span style="font-size:' + Math.round(Math.random()*20+12) + 'px"> </span>');
                });
                words.pop(); // Remove the last space.
                $node.parent()[0].replaceChild($('<span>' + words.join('') + '</span>')[0], node);
            });
        }
    };
});
